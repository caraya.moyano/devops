const express = require('express');
const app = express();
const port = 6000;

app.use(express.json());

app.get('/', (req, res) => {
    return res.status(200).send('OK')
});

app.post('/sumar', (req, res) => {
  const { num1, num2 } = req.body;

  if (typeof num1 !== 'number' || typeof num2 !== 'number') {
    return res.status(400).json({ error: "Los parámetros num1 y num2 son requeridos y deben ser números" });
  }

  const result = num1 + num2;
  res.json({ result });
});

app.post('/multiplicar', (req, res) => {
  const { num1, num2 } = req.body;

  if (typeof num1 !== 'number' || typeof num2 !== 'number') {
    return res.status(400).json({ error: "Los parámetros num1 y num2 son requeridos y deben ser números" });
  }

  const result = num1 * num2;
  res.json({ result });
});

app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`);
});
